# Build a Minecraft Server

Download the v 1.16.something server
https://launcher.mojang.com/v1/objects/35139deedbd5182953cf1caa23835da59ca3d7cd/server.jar

Run it w/ java -jar nogui to create initial config files

Edit them as needed. in one change `eula=false` to `eula=true`

start again and it will run.

Use Xmx and Xms to set initial and max memory

Run w/ nohup or screen
