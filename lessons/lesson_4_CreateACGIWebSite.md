# Create a CGI Website

## Spin up an EC2 Instance
Spin up an instance the same way you did in the last lesson.

This is a test.

You can use ANY resources at your disposal _except_ Lesson 2.

## Introduction to Scripting in Bash

Remember Ye Ol' Circles Diagram?

![ye ol' circles](../images/yeOlCirclesDiagram.png)

There's a reason the shell is immediately after the kernel. It is very powerful.

In addition to being the way users interact with the OS the shell is also
a full programming language. Any shell you are using will be.

We should be using the "Bourne Again Shell" or "bash."

Follow this
https://www.cyberciti.biz/faq/hello-world-bash-shell-script/

replacing nano with vi.

But you don't have to write scripts. You have the power of a full programming language
at your fingertips.

Try this
`for I in 1 2 3 4 5; do echo "Doing step ${I}"; done`

The shell treats a semicolon the same as a newline.
The above is identical to a script with these contents:
```
for I in 1 2 3 4 5
do
  echo "Doing step ${I}"
done
```

This is called a "for loop" because you are running some code `echo "Doing step ${I}"`
for each of the provided elements '1 2 3 4 5'

Went by too fast. Let's add a delay into each step.
`for I in 1 2 3 4 5; do echo "Doing step $I}"; sleep 3; done`

Same elements but now our "code block" has two commands `echo "Doing step $I}"; sleep 3`

What if you don't know how many times you need to iterate.
You can use a while loop

`COUNT=0; while true; do echo "Count has ${COUNT}"; sleep 2; COUNT=$((COUNT+1)); done`

Hit `ctrl+c` to exit the loop. You can put other conditions in there as well.

As a script the above would be
```
COUNT=0
while true
do
  echo "Count has ${COUNT}"
  sleep 2
  COUNT=$((COUNT+1))
done
```

Let's add some conditional statements in there

Try this
`RUNNING=true; COUNT=0; while $RUNNING; do if [[ "$COUNT" -eq "3" ]]; then continue; fi; echo "Count has ${COUNT}"; sleep 2; COUNT=$((COUNT+1)); if [[ "$COUNT" -eq "10" ]]; then RUNNING=false; break; fi; done`

Oh no, why is it stopping at 2?

After hitting `ctrl+c` you should see something like this:
```
[minecraft@localhost ~]$ RUNNING=true; COUNT=0; while $RUNNING; do if [[ "$COUNT" -eq "3" ]]; then continue; fi; echo "Count has ${COUNT}"; sleep 2; COUNT=$((COUNT+1)); if [[ "$COUNT" -eq "10" ]]; then RUNNING=false; break; fi; done
Count has 0
Count has 1
Count has 2
^C
[minecraft@localhost ~]$ 
```

Our program has a bug in it. Let's expand it into what it would be as a script so it is easier to read.


```
RUNNING=true
COUNT=0
while $RUNNING
do
  if [[ "$COUNT" -eq "3" ]]
  then
    continue
  fi
  echo "Count has ${COUNT}"
  sleep 2
  COUNT=$((COUNT+1))
  if [[ "$COUNT" -eq "10" ]]
  then
    RUNNING=false
    break
  fi
done
```


First we set our control variable to true `RUNNING=true` and our count to zero
`COUNT=0`

Then we start our while loop.

```
while $RUNNING
do
```

We have our if test next which appears to be working because we get 0 through 2
before it breaks.

```
if [[ "$COUNT" -eq "3" ]]
  then
```

So it probably isn't our 'if' block. But maybe it is. Let's add an echo statement inside
the if block to see if that is actually being executed.


`RUNNING=true; COUNT=0; while $RUNNING; do if [[ "$COUNT" -eq "3" ]]; then echo "Found 2: skipping"; continue; fi; echo "Count has ${COUNT}"; sleep 2; COUNT=$((COUNT+1)); if [[ "$COUNT" -eq "10" ]]; then RUNNING=false; break; fi; done`

It probably looked pretty similar to the original except that after "Count has 2"
your terminal filled up with "skipping" messages


After hitting `ctrl+c` you should see something like this:
```
[minecraft@localhost ~]$ RUNNING=true; COUNT=0; while $RUNNING; do if [[ "$COUNT" -eq "3" ]]; then continue; fi; echo "Count has ${COUNT}"; sleep 2; COUNT=$((COUNT+1)); if [[ "$COUNT" -eq "10" ]]; then RUNNING=false; break; fi; done
Count has 0
Count has 1
Count has 2
Found 2: skipping
Found 2: skipping
Found 2: skipping
<trunkated>
Found 2: skipping
^C
[minecraft@localhost ~]$ 
```

Yours will be much longer

Do you see what's hapenning. We are using `continue` to skip the rest of the
while loop. That is working as desired however we're doing it four lines
before we increment COUNT. This means that for the next iteration of the
loop COUNT is *STILL* '2'. This is called an infinite loop and it is
one of the more common programming errors.

To solve this lets move the increment before the evaluation (if check.)


`RUNNING=true; COUNT=-1; while $RUNNING; do COUNT=$((COUNT+1)); if [[ "$COUNT" -eq "3" ]]; then continue; fi; echo "Count has ${COUNT}"; sleep 2; if [[ "$COUNT" -eq "10" ]]; then RUNNING=false; break; fi; done`

I've made two other changes. First we set COUNT's initial value to -1 so that
it's still 0 the first time we echo it. Secondly we've added one more echo at
the very end.

Take a look at the expanded code


```
RUNNING=true
COUNT=-1
while $RUNNING
do
  COUNT=$((COUNT+1))
  if [[ "$COUNT" -eq "3" ]]
  then
    continue
  fi
  echo "Count has ${COUNT}"
  sleep 2
  if [[ "$COUNT" -eq "10" ]]
  then
    RUNNING=false
    break
  fi
done
echo "You did it!"
```

Notice that our congratulatory statement happens after the 'done' command
which closes our 'while' loop. This is also referred to as "outside the while loop."

Now your results should look like this:

```
[minecraft@localhost ~]$ RUNNING=true; COUNT=-1; while $RUNNING; do COUNT=$((COUNT+1)); if [[ "$COUNT" -eq "3" ]]; then continue; fi; echo "Count has ${COUNT}"; sleep 2; if [[ "$COUNT" -eq "10" ]]; then RUNNING=false; break; fi; done; echo "You did it!"
Count has 0
Count has 1
Count has 2
Count has 4
Count has 5
Count has 6
Count has 7
Count has 8
Count has 9
Count has 10
You did it!
[minecraft@localhost ~]$ 
```

## Install Apache2

Update apt and install apache2 like you did in lesson 3. You may refer back
to lesson 3.

This time we will also configure a cgi directory.

This tutorial looks good:
http://www.yolinux.com/TUTORIALS/BashShellCgi.html

