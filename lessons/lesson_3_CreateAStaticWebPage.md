# Creating a Static Web Page

## Spin up an EC2 Instance
Spin up an instance the same way you did in the last lesson.

You may need to refer back to Lesson 2.


We are going to follow a modified version of this
https://ubuntu.com/tutorials/install-and-configure-apache#1-overview

Which is actually broken so we'll need to rewrite it.

Connect to the server with ssh

### Installing Apache

run

`sudo apt update`
`sudo apt upgrade apt`
`sudo apt install apache2`

The Ubuntu apache2 package installs apache2 with a configuration file

which specifies a "working directory" of /var/www/html




### Creating your own website.