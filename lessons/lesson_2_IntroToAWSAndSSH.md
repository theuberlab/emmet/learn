# Introduction to Navigating the Command Line


Open git bash

Read chapter 2 in linux for power users https://gitlab.com/theuberlab/linux-unixpowerusers/-/blob/master/Book/Chapter_2.md
At least until the directions stop working.

Now that you have some understanding of the filesystem let's look at all the files in your home directory.

`ls -a`

Do you see one called `.ssh`? Probably not.

first make sure you're in your home directory.

`pwd`

You should see something like

'/c/Users/lforster'

If not run `cd` on it's own. It will change to your home directory.

Now run `ls -a` again. If this is the first time you've done this you probably still won't have an .ssh directory

Let's make one

`mkdir .ssh`


## Connecting to An AWS Instance

### Create the instance

Log in to the AWS console at console.aws.amazon.com

Go to ec2

Go to instances

Create an instance

Choose Ubuntu and t2.micro. Select the "free teir eligible" options.

When it asks you to select a key choose create a new key.

Save the resulting .pem file somewhere you can find it.

Click "view instances" when that is available.

One node will be pending and have a create time of very recently. Click on it.
The instance details will appear below the instance list.

Click on the "tags" tab.

Click "Manage Tags."

Click "Add Tag"

for "Key" input "Name" without quotes and the capitalization must match.
For "value" input something with your name in it so that you can recognize it
in the list. Save your changes.



### Setting Up SSH

switch back to git bash.

Now move that pem file into that directory with mv

`mv /path/to/pem ~/.ssh/` will put it in the '.ssh' directory within your
home directory. '~' is a reference to your home directory.


### Connecting to Your Server
Back in the aws web ui click on the node you recently created.

Click back on the "Details" tab.

Copy the "Public IPv4 address"

Back in git bash run:

`ssh -i ./ssh/<pemfile name>.pem ubuntu@<paste ip address>`

-i is "identity file" it's specifying that we should use this private key
for asymetric key authentication instead of a password.

"ubuntu" is the user you will log in as and the IP is obviously the server
itself. We are using an ssh key to log in because it is more secure. In face
the ubuntu user doesn't even have a valid password. You can only connect
if you have that key.

maybe add some more stuff about looking around on a linux system here.

