# Lesson 1 - Workstation Set Up
Workstation setup for windows users.
If you are on a MacOS or Linux the neccessary software is already installed.


Must have Windows 7 or higher


# Install Some Softwares

## git

Download the 64 bit "Git for Windows Setup" from here https://git-scm.com/download/win

For Install type choose full.

During the install when asked:

If you want to install "Git bash" say yes. It might be "Adjusting your PATH environment." And the option is "Use git bash only."

When asked which terminal emulator to use with Git Bash choose MinTTY

For "Configuring extra options"
  "Enable file system caching" - no
  "Enable git credential manager" - Yes
  "Enable symbolic links' - No



## Slack

You should have received an invitation to a slack workspace called "TheUberlab."

Download slack from here https://slack.com/downloads/windows

Once installed it will show you what looks like a login window. But this
will actually create a new workspace. Go back to the email from slack inviting
you to TheUberlab and follow the directions there.


## Visual Studio Code
NOTE: This is actually different than "Visual Studio."

Get it from here
https://code.visualstudio.com/download



# Check Out the Learn Repository

First log in to gitlab and navigate to the learn project.
https://gitlab.com/theuberlab/emmet/learn

NOTE: If you used one of the "Login using..." options to login using google,  
facebook or any other then the first time you log in you should see a message
at the top of your window which says something like 'You will not be able to clone
repositories until you set a password.' It will have a link to the set password
page. Do this.

Open Git Bash

In git bash execute the following 3 commands:

`mkdir -p <path to docs>/code/gitlab.com/theuberlab/emmet`
`cd <path to docs>/code/gitlab.com/theuberlab/emmet`
`git clone https://gitlab.com/theuberlab/emmet/learn.git`


