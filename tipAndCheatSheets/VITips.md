# VI Cheat Sheet

## Overview of VI

VI (vi) is a command line text editor present on almost all Unix and Linux machines.

Unlike most GUI editors you are familiar with vi expects you to be using
your keyboard for both text input and issuing commands (copy, paste, etc.)

There are other editors that are also intended for command line (no gui)
use (the most popular being EMACS) each one has it's own quirks.

The most immediate difference between vi and any other editor you have likely
ever used is that instead of using combinations of keys to issue commands
(such as ctrl+c to copy text in a windows GUI editor) vi switches between
primarily two (actually 3) modes.

Officially these are:
  * Insert mode - Where you can add text.
  * Command mode - Where you issue commands such as yank (copy) and put (paste)
  * Execution mode - While in command mode you can type a colon ":" and issue
    a number of more involved commands such as find/replace, executing
    an external command. This is also how you save and quit.

Unofficially it is simpler to think of vi having two modes.

  * Mangle mode - Where you add some garbage to your text
  * Beep mode - Where vi beeps at you when you type invalid command keys
    and where you can do things like search and save.

Vi has a number of incredibly powerful features such as automatic
text replacement and aliases which can be used to string together a
series of commands. None of which will be covered here.

The following covers a number of very useful beep mode commands.

## Essential Commands You May Not Know
These commands belong somewhere else but they are vital. You may not know
them or you may be used to some other way to do the same thing.

  * :e! - Reload the file from disk. When you have accidentally pasted text
    into your terminal while in beep mode and no longer fully know what
    weird changes you may have made to the file, `:e!` will reload from disk.
  * :wq - A combination of write (`:w`) and quit (`:q`) Avoid the temptation
    to use the shortcut ZZ to save and quit. You may accidentally hit
    `ctrl z` which will put vi in the background and return you to the
    command line giving you the false impression that your changes have
    been saved when in fact they have not.
  * :q! - Force quit without saving
  * / and ? - Search for a string forward '/' or backwards '?' - Moves
    your cursor to the first occurrance of your search
  * n - Move to the next match
  * . - Perform the last command
  * c - Change - like `y` (yank) and `d` (delete) see below for more detail.

## Movement Commands
These commands are for getting around in a document.

  * hjkl - Like wasd but in vi
  * G - Go to the last line of the file
  * _n_ G - Go to line _n_ of the file.
  * ^ - Go to the beginning of the current line
  * $ - Go to the end of the current line
  * w - move forward one "word". A "word" is a sequence of simple ascii
    characters (read non-special chars)
  * W - move forward one word but only treat white space as a word delimiter
  * b - Move backwards one word
  * B - Move backwards one word but only treat white space as a word delimiter

## Entering Mangle Mode
The following six commands are the most common way to enter mangle mode.

  * i - Enter mangle mode at the position immediately to the left of the cursor
  * I - Enter mangle mode at the position furthest left of the cursor
  * a - Enter mangle mode at the position immediately to the right of the cursor
  * A - Enter mangle mode at the position furthest right of the cursor
  * o - Insert a new line below the cursor and enter mangle mode
  * O - Insert a new line above the cursor and enter mangle mode

## Basic Editing Commands
These commands change text and typically involve moving you from beep mode
to mangle mode.

  * y _n_ y - Yank (copy) _n_ lines. _n_ may be ommited to yank only the current line
  * d _n_ d - Delete (cut) _n_ lines. _n_ may be ommited to delete only the current line
  * c _n_ c - Change text. Removes the current line and puts you into mangle mode in it's place
  * p - Put (paste) previously yanked or deleted text

## More Powerful Editing Commands
This is where vi becomes amazing.

The real power comes when you realize that you can combine editing commands
with numbers and with any movement commands.

Such as:
  * yw - Yank the word under the cursor
  * y4w - Yank the word under the cursor and the subsequent 3 words
  * d$ - Delete everything from the character under the cursor to the end of the line
  * c2W - Change the next two "words" where whitespace is the word delimiter
  * d12G - Delete the current line as well as all lines up to and including
    line 12. Note this works backwards as well as forwards
