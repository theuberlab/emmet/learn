# Definitions
Common terms used in engineering.



* \# - Pound
* \! - Bang
* \| - Pipe
* \>\< - Redirect
* \. - Dot
* \* - Star
* \[ - Square bracket
* \{ - Curly brace
* \( - Paren
* \` - Grave accent
* \~ - Tilde
* \' - Single quote
* \" - Double quote
* \`\" - \"Smart quotes\"
* Special characters - Characters which are interpreted by the shell to mean something different. Basically everything that isn't a letter or a number.
* Escaping special characters - Ways to get the shell to treat special characters as if they were simply text
* UI - User Interface.
* CLI - Command Line Interface
* GUI - Graphical User Interface
* OS - Operating System
* Shell - A user's primary interface to the operating system (but also more.)
* Terminal - The program you run to interact with your shell. These used to be physical machines. Just a screen and a keyboard with no intelligence. The software these days might be called a terminal emulator.
* Command - Something you execute to perform a result. Ex. `ls` to see a list of the contents of a directory.
* Script - A collection of commands that can be executed as if it were a single command. A very simple form of program.
* STDOUT - "Standard Out" The channel upon which commands send their output. By default STDOUT is connected to your terminal.
* STDIN - The channel upon which all commands recieve input. Not connected by default for most applications however your shell's STDIN is connected to your terminal.
* STDERR - The channel upon which commands send error output. By default STDERR is also connected to your terminal.
* Program, Application, Executable -
* Binary - A common term for native executables. I.E. they are in a native binary format, not ASCII files which can be edited. 
* Daemon - A long lived program that runs in the background not attached to your terminal.
* Permissions
* Exploit
* Overloaded - Overloaded terms
* PID
* GID
* Arguments
* Flags
* Command - Command line commands, "sub command" commands
* EOF
* EOL
* /dev/null - The bit bucket
* Attributes
* Parameters
* Project
* TLA
* Workflow
* Convention
* Standard
* RFC
* Protocol
* Config file
* log
* directory/folder
* AuthN/Z
* Programming language
* Scripting language
* Markup language
* Config syntax
* Text editor
* IDE
* Terminate/Kill
* Signal - SIGTERM, SIGABRT
* Stack trace
* User - Your "user", "users"
* User space/user land
* Containers
* Docker
* Utilization - Memory, CPU, Disk, Network
* IO
* URL
* URI
* Scheme
* CGI
* CGI Parameters
* Headers
* Body
* Buzzword Bingo
* Hack
* Hacking
* BOFH
* PFY
* Stack overflow
* Google
* DSL - Domain Specific Language
* TODO
* Issue
* Incident
* Ticket
* Declarative (Configuration)
* Legacy (software)
* Cloud
* Cloud ready
* Microservice
* Local
* Remote
* Client
* Server
* RTFM

## Networking Terms

* Protocol
* Ethernet
* IP
* TCP
* UDP
* DNS
* TCP/IP
* OSI Model
* TCP Model
* Layers

## Programming Specific Terms
Some terms specific to programming with examples of what they look
like in some common languages

* Variable
* Literal
* Type
* Array
* Hash
* String
* Heap
* Stack
* OO
* Imperative
* Library
* Framework
* API - Both usages
* 

## CI/CD Terms

* Development Process/Development Workflow
* Commit
* Push
* Merge
* PR/MR
* Approve
* Compile
* Build
* Release
* Deploy - Slightly different than release
* CI/CD
* Pipeline
* Job
* Workflow
* Environment

## Certifications
Just to give an idea that it's a thing.

* Certification
* RCE
* CCIE
* MCSE
* CompTIA
